package com.example.nvoclientes;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
//@RequestMapping("/apitecu/v2/datos")
public class ClientesController {


    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/tokenget")
    public String tokenget(@RequestParam(value="nombre", defaultValue="Tech U!") String name){
        //jwtBuilder = new JWTBuilder();
        return jwtBuilder.generateToken(name,"admin");
    }

    @GetMapping(path="/hello",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public String helloWorld(){
        String s = "Hello from JWT demo...";
        return s;
    }


    @GetMapping(path = "/datos",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    //public String getClientes(@RequestParam(value="Busca",required = false,defaultValue = "") String cteBusca) {
    public List getClientes(@RequestBody String cteBusca) {
        return ClientesService.getCte(cteBusca);

    }

    @PostMapping(path="/datos", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public String setcliente(@RequestBody String newCliente) {
        try {
            ClientesService.insert(newCliente);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @PutMapping(path = "/datos",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public String updCliente(@RequestBody String updDato) {
        try {
            JSONObject obj = new JSONObject(updDato);//
            String filtro = obj.getJSONObject("filtro").toString();
            String updates = obj.getJSONObject("updates").toString();
            ClientesService.actualiza(filtro, updates);
        } catch (Exception ex) {
            return ex.getMessage();
        } return "Cliente actualizado";

    }
}



