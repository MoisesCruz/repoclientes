package com.example.nvoclientes;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ClientesService {
    static MongoCollection<Document> clientes; //variable que apunta a nuestra coleccion de estructura Document
    private static MongoCollection<Document> getClientesColection(){
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings =  MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo = MongoClients.create(settings); // herr
        MongoDatabase database = mongo.getDatabase("dbprod");
        return database.getCollection("clientes");

    }

    public static void insert(String insClientes) throws Exception{
        clientes = getClientesColection();
        Document docCtes = Document.parse(insClientes); //transnforma en documento insertable y tratable para mongo
        clientes.insertOne(docCtes);
    }

   /* public String valida(String valCte,String pw) throws  Exception{
        clientes =getClientesColection(); //accede a la coleccion
        List lis = new ArrayList(); //preparamos el arreglo
        Document docfiltro = Document.parse(valCte);
        Document docfiltro2 = Document.parse(pw);
        FindIterable<Document> iterDoc = clientes.find(docfiltro); // nos devuelve la lista de documentos
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            lis.add(it.next());
        }
        return lis;
    }
*/


    public static List getAll(){
        clientes = getClientesColection(); //llamada a la coleccion
        List lis = new ArrayList(); //arreglo donde ingresara nuestros elementos
        FindIterable<Document> docuIterado = clientes.find(); // variable guarda la busqueda de todos los elementos en mongo
        Iterator it = docuIterado.iterator();
        while (it.hasNext()){
            lis.add(it.next());
        }
        return lis;
    }

    public static List getCte(String cteBuscado){
        clientes =getClientesColection(); //accede a la coleccion
        //String str = String.format("{'cliente':'%s'}",cteBuscado);
        List lis = new ArrayList(); //preparamos el arreglo
        Document docfiltro = Document.parse(cteBuscado);
        FindIterable<Document> iterDoc = clientes.find(docfiltro); // nos devuelve la lista de documentos
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            lis.add(it.next());
        }
        return lis;
    }

    public static void actualiza(String filtro, String updates){
        clientes =getClientesColection();
        Document docFiltro =  Document.parse(filtro);
        Document doc = Document.parse(updates);
        clientes.updateOne(docFiltro,doc);//asignara los valores que este en la variable doc por medio del docFiltro encontratra a quien actualizra
    }


}
