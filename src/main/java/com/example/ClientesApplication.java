package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.kastkode.springsandwich.filter","com.example.*"})
public class ClientesApplication {


	public static void main(String[] args) {
		SpringApplication.run(ClientesApplication.class, args);
	}

}
